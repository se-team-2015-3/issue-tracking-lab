program Lab3;

{$mode objfpc}{$H+}

uses
    MatrixManager;
var
    matrix: MatrixSquare;
    coordinate: CoordinateElement;
begin
    Randomize;
    FillMatrix(matrix);
    WriteLn('First matrix: ');
    PrintMatrix(matrix);
    coordinate := GetCoordinateMaxOnSecondary(matrix);
    WriteLn('Coordinate: ', 'line ', coordinate.line, ' column ', coordinate.column);
    WriteLn('Element: ', matrix[coordinate.line, coordinate.column]:6:2);
    WriteLn('Sum elements: ', GetAverageSumElement(matrix):6:2);
    WriteLn();
    WriteLn('Second matrix: ');
    Sort(matrix);
    PrintMatrix(matrix);
    ReadLn;
end.

