unit MatrixManager;

{$mode objfpc}{$H+}

interface
const
     MaxElement = -6;
     MinElement = 4;
     MatrixSize = 6;
     MatrixAmount = MaxElement - MinElement + 1;
type
     CoordinateElement = record
         line: Integer;
         column: Integer;
     end;

     MatrixSquare = array[1..MatrixSize, 1..MatrixSize] of Real;

procedure FillMatrix(var matrix: MatrixSquare);
procedure PrintMatrix(matrix: MatrixSquare);
function GetAverageSumElement(matrix: MatrixSquare): Real;
function GetCoordinateMaxOnSecondary(matrix: MatrixSquare): CoordinateElement;
procedure Sort(var matrix: MatrixSquare);

implementation

    procedure FillMatrix(var matrix: MatrixSquare);
    var
        i, j: Integer;
    begin
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                matrix[i, j] := random(MatrixAmount) + MinElement;
            end;
        end;
    end;

    procedure PrintMatrix(matrix: MatrixSquare);
    var
        i, j: Integer;
    begin
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                Write(matrix[i, j]:6:2);
            end;
            WriteLn;
            WriteLn;
       end;
    end;
    
    function GetCoordinateMaxOnSecondary(matrix: MatrixSquare): CoordinateElement;
    var
        maxElement: Real;
        i: Integer;
        coordinateMaxElement: CoordinateElement;
    begin
        maxElement := matrix[1, MatrixSize];
        coordinateMaxElement.line := 1;
        coordinateMaxElement.column := MatrixSize;
        for i := 1 to MatrixSize do
        begin
            if matrix[i, MatrixSize - i + 1] > maxElement then
            begin
                maxElement := matrix[i, MatrixSize - i + 1];
                coordinateMaxElement.line := i;
                coordinateMaxElement.column := MatrixSize - i + 1;
            end;
        end;
        GetCoordinateMaxOnSecondary := coordinateMaxElement;
    end;

    function GetAverageSumElement(matrix: MatrixSquare): Real;
    var
        i, j, countElements: Integer;
        averageSum: Real;
        sum: Real;
    begin
        sum := 0;
        countElements := 0;
        for i := 1 to MatrixSize do
        begin
            for j := 1 to MatrixSize do
            begin
                if i > j then
                begin
                    countElements := countElements + 1;
                    sum := sum + matrix[i, j];
                end;
            end;
        end;
        averageSum := sum / countElements;
        GetAverageSumElement := averageSum;
    end;

    procedure Sort(var matrix: MatrixSquare);
    var
        coordinateMaxElement: CoordinateElement;
    begin
        coordinateMaxElement := GetCoordinateMaxOnSecondary(matrix);
        matrix[coordinateMaxElement.line, coordinateMaxElement.column] := GetAverageSumElement(matrix);
    end;
end.
